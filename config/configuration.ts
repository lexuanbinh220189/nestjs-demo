import { merge } from 'lodash';
import { defaultConfig } from './default.config';

export const configuration = async () => {
	const envConfigModule = await import(
		`./${process.env.ENVIRONMENT}.config.js`
	);

	const envConfig = envConfigModule.configuration;
	return merge(defaultConfig, envConfig, process.env);
};
