export enum UserStatus {
	Active = 'Active',
	Suspended = 'Suspended',
	Locked = 'Locked',
}

export enum Gender {
	Male = 'Male',
	Female = 'Female',
	Other = 'Other',
}
