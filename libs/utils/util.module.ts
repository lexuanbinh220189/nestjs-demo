import { Module } from '@nestjs/common';
import { Utils } from './util.service';

@Module({
	imports: [],
	exports: [Utils],
	providers: [Utils],
})
export class UtilsModule {}
