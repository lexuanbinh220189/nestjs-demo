import { Controller, Get, Post, Req, UseGuards, Body } from '@nestjs/common';
import { plainToClass } from 'class-transformer';

// Guard
import { LocalAuthGuard } from './guards/local-auth.guard';
import { JwtAuthGuard } from './guards/jwt-auth.guard';

// Service
import { AuthService } from './auth.service';
import { UserService } from '../user/user.service';
import { Utils } from '../../libs/utils/util.service';

// Entity
import { User } from '../user/user.entity';

import { AuthUser } from '../decorators/auth-user.decorator';
import { CreateUserDto } from '../user/dto/create-user.dto';
import { HttpErrors } from '../../libs/const/httpMessages.const';

@Controller('auth')
export class AuthController {
	constructor(
		private readonly authService: AuthService,
		private readonly userService: UserService,
		private readonly httpErrors: HttpErrors,
		private readonly utils: Utils,
	) {}

	@UseGuards(LocalAuthGuard)
	@Post('/login')
	login(@Req() req: Request | any): Promise<{ accessToken: string }> {
		return this.authService.generateJwtToken(req.user);
	}

	@UseGuards(JwtAuthGuard)
	@Get('/me')
	async myProfile(
		@Req() req: Request | any,
		@AuthUser() authUser: any,
	): Promise<any> {
		const user = await this.userService.findById(authUser.sub);

		return {
			...plainToClass(User, user),
			authUser,
		};
	}

	@Post('/register')
	async create(@Body() userData: CreateUserDto): Promise<User> {
		try {
			await this.userService.saveTransaction(userData);
			return plainToClass(User, userData);
		} catch (err) {
			if (
				this.utils.getProp(err, 'code', null) ==
				this.httpErrors.getErrors().DuplicateDatabase.code
			) {
				throw this.httpErrors.getErrorResponse('DuplicateDatabase');
			}

			throw this.httpErrors.getErrorResponse('Generic');
		}
	}
}
