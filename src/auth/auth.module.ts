import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';

// Module
import { UserModule } from '../user/user.module';
import { UserProfileModule } from '../userProfile/user.profile.module';
import { ConstModule } from '../../libs/const/const.module';
import { UtilsModule } from '../../libs/utils/util.module';

// Controller
import { AuthController } from './auth.controller';

// Service
import { AuthService } from './auth.service';

// Repository
import { UserRepository } from '../user/user.repository';
import { UserProfileRepository } from '../userProfile/user.profile.repository';

// Entity
import { User } from '../user/user.entity';
import { UserProfile } from '../userProfile/user.profile.entity';

// Other
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtStrategy } from './strategies/jwt.strategy';

@Module({
	imports: [
		JwtModule.registerAsync({
			imports: [ConfigModule],
			useFactory: async (configService: ConfigService) => ({
				secret: configService.get<string>('JWT_SECRET_KEY'),
			}),
			inject: [ConfigService],
		}),
		TypeOrmModule.forFeature([
			User,
			UserRepository,
			UserProfile,
			UserProfileRepository,
		]),
		UserModule,
		UserProfileModule,
		PassportModule,
		ConfigModule,
		ConstModule,
		UtilsModule,
	],
	providers: [AuthService, LocalStrategy, JwtStrategy],
	controllers: [AuthController],
})
export class AuthModule {}
