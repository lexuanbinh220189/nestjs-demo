import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	const configService = app.get(ConfigService);
	const PORT = configService.get('PORT') ?? 3000;
	app.setGlobalPrefix(configService.get('API_PREFIX') ?? '/v1');
	app.useGlobalPipes(
		new ValidationPipe({
			disableErrorMessages: configService.get('DISABLE_ERROR_MESSAGE'),
			whitelist: configService.get('WHITE_LIST'),
		}),
	);

	await app.listen(PORT, () =>
		console.log(`Application running on port ${PORT}`),
	);
}
bootstrap();
