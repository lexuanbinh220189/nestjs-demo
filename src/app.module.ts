import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { ConfigService } from '@nestjs/config';

import { AppService } from './app.service';
import { configuration } from '../config/configuration';

// Module
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { UserProfileModule } from './userProfile/user.profile.module';
import { ConstModule } from '../libs/const/const.module';
import { UtilsModule } from '../libs/utils/util.module';

//Entity
import { User } from './user/user.entity';
import { UserProfile } from './userProfile/user.profile.entity';

import { SnakeNamingStrategy } from '../libs/database/snake-naming.strategy';

@Module({
	imports: [
		ConfigModule.forRoot({
			isGlobal: true,
			envFilePath: `./enviroment/.${process.env.NODE_ENV}.env`,
			load: [configuration],
		}),
		TypeOrmModule.forRootAsync({
			imports: [ConfigModule],
			useFactory: (configService: ConfigService) => ({
				type: 'postgres',
				host: configService.get('DATABASE_HOST'),
				port: +configService.get('DATABASE_PORT'),
				username: configService.get('DATABASE_USERNAME'),
				password: configService.get('DATABASE_PASSWORD'),
				database: configService.get('DATABASE_DB_NAME'),
				namingStrategy: new SnakeNamingStrategy(),
				migrations: ['dist/migrations/*.{ts,js}', 'src/migrations/*.{ts,js}'],
				entities: [User, UserProfile],
				migrationsRun: false,
				logging: true,
				migrationsTableName: 'migration',
				synchronize: true,
				cli: {
					migrationsDir: 'dist/migrations',
				},
			}),
			inject: [ConfigService],
		}),
		AuthModule,
		UserModule,
		UserProfileModule,
		ConstModule,
		UtilsModule,
	],
	controllers: [],
	providers: [AppService],
})
export class AppModule {}
