import { Injectable } from '@nestjs/common';
import * as _ from 'lodash';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';

import { BaseAbstractService } from '../../libs/service/base.service';
import { UserRepository } from './user.repository';

// Entity
import { User } from './user.entity';
import { UserProfile } from '../userProfile/user.profile.entity';

@Injectable()
export class UserService extends BaseAbstractService<User, UserRepository> {
	constructor(
		repository: UserRepository,
		@InjectEntityManager()
		private readonly entityManager: EntityManager,
	) {
		super(repository);
	}

	public saveTransaction(data: any): Promise<any> {
		return this.repository.manager.connection.transaction(async () => {
			// define repository want to use
			const userRepository = this.entityManager.getRepository(User);
			const userProfileRepository =
				this.entityManager.getRepository(UserProfile);

			const _user: any = userRepository.create(
				_.pick(data, [
					'emailAddress',
					'firstName',
					'lastName',
					'password',
					'status',
					'areaCode',
					'mobileNumber',
					'isVerified',
					'deviceId',
					'deviceFcmToken',
				]),
			); // need create before save this issue TypeORM
			const userInserted = await userRepository.save(_user);

			const userProfileInserted: any = await userProfileRepository.save({
				..._.pick(data, [
					'user',
					'bankBranch',
					'bankName',
					'bankNumber',
					'birthDate',
					'address',
					'gender',
					'tax',
				]),
				user: userInserted.id,
			});

			userRepository.update(
				{ id: userInserted.id },
				{ profile: userProfileInserted.id },
			);
		});
	}

	async findByEmail(email: string): Promise<User | null> {
		return this.repository.findOneBy({ emailAddress: email });
	}

	getInactiveUsers(): Promise<User[]> {
		return this.repository.getInactiveUsers();
	}
}
