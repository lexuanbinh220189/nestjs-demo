import { Repository } from 'typeorm';
import { User } from './user.entity';
import { CustomRepository } from 'libs/repository/typeorm-ex.decorator';

@CustomRepository(User)
export class UserRepository extends Repository<User> {
	getInactiveUsers(): Promise<User[]> {
		return this.createQueryBuilder()
			.where('isActive = :active', { active: false })
			.getMany();
	}
}
