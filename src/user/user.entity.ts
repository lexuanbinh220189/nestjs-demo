import {
	BeforeInsert,
	BeforeUpdate,
	Column,
	Entity,
	JoinColumn,
	OneToOne,
	PrimaryGeneratedColumn,
} from 'typeorm';
import { Exclude, Expose } from 'class-transformer';
import { AbstractEntity } from '../../libs/abstract.entity';
import { UserStatus } from '../../libs/enums/index';
import { UserProfile } from '../userProfile/user.profile.entity';
import { Utils } from '../../libs/utils/util.service';

@Entity({ name: 'Users' })
export class User extends AbstractEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ type: 'varchar', length: 150, nullable: false, unique: true })
	emailAddress: string;

	@Column()
	firstName: string;

	@Column()
	lastName: string;

	@Exclude()
	@Column()
	password: string;

	@Exclude()
	@Column({ type: 'varchar', length: 255, nullable: true })
	deviceId: string;

	@Exclude()
	@Column({ type: 'varchar', length: 255, nullable: true })
	deviceFcmToken: string;

	@Column({ type: 'varchar', length: 3 })
	areaCode: string;

	@Column({ type: 'varchar', length: 16, unique: true })
	mobileNumber: string;

	@Column({ type: 'enum', enum: UserStatus, default: UserStatus.Active })
	status: string;

	@Column({ default: true })
	isVerified: boolean;

	@OneToOne(() => UserProfile, (_userProfile) => _userProfile.id)
	@JoinColumn({ name: 'profile' })
	profile: UserProfile;

	constructor(partial: Partial<User>) {
		super();
		Object.assign(this, partial);
	}

	@Expose()
	get fullName(): string {
		return `${this.firstName} ${this.lastName}`;
	}

	@BeforeInsert()
	@BeforeUpdate()
	async hashPassword(): Promise<void> {
		const utils = new Utils();
		this.password = await utils.hashPassword(this.password);
	}
}
