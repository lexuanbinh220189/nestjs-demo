import { Transform } from 'class-transformer';
import {
	IsBoolean,
	IsEmail,
	IsNotEmpty,
	IsOptional,
	Length,
	Matches,
} from 'class-validator';
import { Match } from '../../../libs/validations/match.validation';

export class CreateUserDto {
	@IsNotEmpty()
	@IsEmail()
	emailAddress: string;

	@IsNotEmpty()
	firstName: string;

	@IsNotEmpty()
	lastName: string;

	@IsNotEmpty()
	@Length(8, 24)
	@Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
		message:
			'password must include at least one uppercase and one special character',
	})
	password: string;

	@IsNotEmpty()
	@Length(8, 24)
	@Match<CreateUserDto>('password')
	confirmPassword: string;

	@IsNotEmpty()
	@Length(1, 3)
	areaCode: string;

	@IsNotEmpty()
	@Length(9, 16)
	mobileNumber: string;

	@IsOptional()
	@IsBoolean()
	@Transform(({ value }) => value === 'true') // issue with class-transformer, use this to convert
	isVerified: boolean;

	@IsOptional()
	bankBranch: string;

	@IsOptional()
	bankName: string;

	@IsOptional()
	bankNumber: string;

	@IsOptional()
	birthDate: 'datetime';

	@IsOptional()
	address: string;

	@IsOptional()
	gender: string;

	@IsOptional()
	tax: string;
}
