import { Module } from '@nestjs/common';

// Module
import { TypeOrmExModule } from '../../libs/repository/typeorm-ex.module';
import { UserProfileModule } from '../userProfile/user.profile.module';
import { UtilsModule } from '../../libs/utils/util.module';

// Service
import { UserService } from './user.service';

// Repository
import { UserRepository } from './user.repository';
import { UserProfileRepository } from '../userProfile/user.profile.repository';

// Entity
import { User } from './user.entity';
import { UserProfile } from '../userProfile/user.profile.entity';

@Module({
	imports: [
		TypeOrmExModule.forCustomRepository([
			User,
			UserRepository,
			UserProfile,
			UserProfileRepository,
		]),
		UserProfileModule,
		UtilsModule,
	],
	providers: [UserService],
	exports: [UserService, TypeOrmExModule],
})
export class UserModule {}
