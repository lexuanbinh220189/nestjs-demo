import { Injectable } from '@nestjs/common';
import { BaseAbstractService } from '../../libs/service/base.service';
import { UserProfile } from './user.profile.entity';
import { UserProfileRepository } from './user.profile.repository';

@Injectable()
export class UserProfileService extends BaseAbstractService<
	UserProfile,
	UserProfileRepository
> {
	constructor(repository: UserProfileRepository) {
		super(repository);
	}
}
