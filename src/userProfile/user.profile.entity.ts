import {
	Column,
	Entity,
	JoinColumn,
	OneToOne,
	PrimaryGeneratedColumn,
} from 'typeorm';
import { AbstractEntity } from '../../libs/abstract.entity';
import { Gender } from '../../libs/enums/index';
import { User } from '../user/user.entity';

@Entity({ name: 'UserProfiles' })
export class UserProfile extends AbstractEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ type: 'varchar', length: 255, nullable: true })
	bankBranch: string;

	@Column({ type: 'varchar', length: 255, nullable: true })
	bankName: string;

	@Column({ type: 'varchar', length: 255, nullable: true })
	bankNumber: string;

	@Column({ nullable: true })
	birthDate: 'datetime';

	@Column({ type: 'varchar', length: 255, nullable: true })
	address: string;

	@Column({ type: 'enum', enum: Gender, default: Gender.Other })
	gender: string;

	@Column({ type: 'varchar', length: 100, nullable: true })
	tax: string;

	@OneToOne(() => User, (_user) => _user.id)
	@JoinColumn({ name: 'user' })
	user: User;

	constructor(partial: Partial<UserProfile>) {
		super();
		Object.assign(this, partial);
	}
}
