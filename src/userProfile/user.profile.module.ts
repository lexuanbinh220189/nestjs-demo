import { Module } from '@nestjs/common';
import { TypeOrmExModule } from '../../libs/repository/typeorm-ex.module';
import { UserProfileRepository } from './user.profile.repository';
import { UserProfile } from './user.profile.entity';
import { UserProfileService } from './user.profile.service';

@Module({
	imports: [
		TypeOrmExModule.forCustomRepository([UserProfile, UserProfileRepository]),
	],
	providers: [UserProfileService],
	exports: [UserProfileService, TypeOrmExModule],
})
export class UserProfileModule {}
