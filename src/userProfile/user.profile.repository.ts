import { Repository } from 'typeorm';
import { UserProfile } from './user.profile.entity';
import { CustomRepository } from 'libs/repository/typeorm-ex.decorator';
import { Injectable } from '@nestjs/common';

@Injectable()
@CustomRepository(UserProfile)
export class UserProfileRepository extends Repository<UserProfile> {}
